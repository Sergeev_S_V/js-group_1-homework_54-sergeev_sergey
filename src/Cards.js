import React from 'react';
import Card from './Card';

const Cards = props => {
    return (
        <div className='cardsBox'>
            <div className='btnBox'><button onClick={props.shuffle}>Shuffle Cards</button></div>
            {props.cards.map((card, index) => <Card rank={card.rank} suit={card.suit} key={index}/>)}
        </div>
    );
};

export default Cards;