import React from 'react';

const CurrentHand = (props) => {
    return (
        <div className='currentHandBox'>{props.currentHand}</div>
    );
};

export default CurrentHand;