import React, { Component } from 'react';
import './App.css';
import Cards from "./Cards";
import CurrentHand from './CurrentHand';
import CheckCombination from './CheckCombination';

class App extends Component {

    ranks = [2,3,4,5,6,7,8,9,10,'J','Q','K','A'];
    suits = ['D', 'H', 'S', 'C'];



    state = {
        cards: [],
        currentHand: 'Nothing'
    };


    shuffle = () => {
        let i = 5;
        let cards = [];
        while (i > 0) {
            let rank = this.ranks[Math.floor(Math.random() * this.ranks.length)];
            let suit = this.suits[Math.floor(Math.random() * this.suits.length)];
            let card = {rank: rank, suit: suit};
            let filterCard = (card) => {
                return card.rank === rank && card.suit === suit
            };
            let newCards = cards.filter(filterCard);
            if (newCards.length > 0) {
                continue;
            }
            cards.push(card);
            i--;
        }
        console.log(cards);
        CheckCombination(cards);
        this.setState({cards});
    };



  render() {
    return (
      <div className="App">
        <div className="playingCards faceImages">
            <Cards shuffle={this.shuffle} cards={this.state.cards}/>
        </div>
          <CurrentHand currentHand={this.state.currentHand}/>

      </div>
    );
  }
}

export default App;
