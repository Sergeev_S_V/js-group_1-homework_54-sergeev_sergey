import React from 'react';

const suits = {
    'D': {name: 'diams', symbol: '♦'},
    'H': {name: 'hearts', symbol: '♥'},
    'S': {name: 'spades', symbol: '♠'},
    'C': {name: 'clubs', symbol: '♣'}
};

const Card = (props) => {
        const rank = props.rank;
        const suit = props.suit;
        const suitName = suits[suit].name;
        const suitSymbol = suits[suit].symbol;

        let className = `card rank-${rank} ${suitName}`;
        return (
            <div className={className}>
                <span className='rank'>{props.rank}</span>
                <span className='suit'>{suitSymbol}</span>
            </div>
        );
};

export default Card;